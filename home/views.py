from django.shortcuts import render

#this is the views
def index(request):
    context = {}
    return render(request, "home/index.html", context)